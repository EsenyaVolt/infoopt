﻿namespace Information_service_of_a_wholesale_company
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_Position = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Password_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Input = new System.Windows.Forms.Button();
            this.textBox1_Otdel = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBox_Position
            // 
            this.comboBox_Position.FormattingEnabled = true;
            this.comboBox_Position.Location = new System.Drawing.Point(20, 7);
            this.comboBox_Position.Name = "comboBox_Position";
            this.comboBox_Position.Size = new System.Drawing.Size(269, 28);
            this.comboBox_Position.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(295, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Отдел";
            // 
            // Password_TextBox
            // 
            this.Password_TextBox.Location = new System.Drawing.Point(20, 46);
            this.Password_TextBox.Name = "Password_TextBox";
            this.Password_TextBox.Size = new System.Drawing.Size(269, 26);
            this.Password_TextBox.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 24;
            this.label2.Text = "Пароль";
            // 
            // Input
            // 
            this.Input.Location = new System.Drawing.Point(322, 75);
            this.Input.Name = "Input";
            this.Input.Size = new System.Drawing.Size(129, 31);
            this.Input.TabIndex = 25;
            this.Input.Text = "Вход";
            this.Input.UseVisualStyleBackColor = true;
            this.Input.Click += new System.EventHandler(this.Input_Click);
            // 
            // textBox1_Otdel
            // 
            this.textBox1_Otdel.Location = new System.Drawing.Point(20, 80);
            this.textBox1_Otdel.Name = "textBox1_Otdel";
            this.textBox1_Otdel.Size = new System.Drawing.Size(269, 26);
            this.textBox1_Otdel.TabIndex = 26;
            this.textBox1_Otdel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 117);
            this.Controls.Add(this.textBox1_Otdel);
            this.Controls.Add(this.Input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Password_TextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Position);
            this.Name = "Form1";
            this.Text = "Вход в систему";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_Position;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox Password_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Input;
        public System.Windows.Forms.TextBox textBox1_Otdel;
    }
}

