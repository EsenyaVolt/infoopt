﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 
using System.Data.SqlClient;

namespace Information_service_of_a_wholesale_company
{
    public partial class Warehouse : Form
    {
        public Warehouse()
        {
            InitializeComponent();
        }

        private const string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Елена\\Desktop\\informationserviceofawholesalecompany\\Information_service_of_a_wholesale_company\\Information_service_of_a_wholesale_company\\information_service.mdf;Integrated Security=True";
        
        void updateWarehouseDGV()
        {
            var request = @"SELECT * FROM Product JOIN Product_has_Warehouse
                                ON Product.Id=Product_has_Warehouse.product_id
                                JOIN Warehouse
                                ON Warehouse.Id=Product_has_Warehouse.warehouse_id
                                ";
            var adapter = new SqlDataAdapter(request, connectionString);
                DataTable warehouseTable = new DataTable();
                adapter.Fill(warehouseTable);
                warehouse_dgv.DataSource = warehouseTable;

            warehouse_dgv.Columns["Id"].Visible = false;
            warehouse_dgv.Columns["Id1"].Visible = false;
            warehouse_dgv.Columns["Warehouse_id"].Visible = false;
            warehouse_dgv.Columns["product_id"].Visible = false;
            warehouse_dgv.Columns["warehouse_id1"].Visible = false;

            warehouse_dgv.Columns["NameOfItem"].HeaderText = "Наименование";
            warehouse_dgv.Columns["View"].HeaderText = "Единицы измерения";
            warehouse_dgv.Columns["Made"].HeaderText = "Дата поступления";
            warehouse_dgv.Columns["ValidUntil"].HeaderText = "Дата истечения срока годности";
            warehouse_dgv.Columns["Quantity"].HeaderText = "Количество";
            warehouse_dgv.Columns["purchasePrice"].HeaderText = "Цена закупки";
            warehouse_dgv.Columns["StorageTemperature"].HeaderText = "Температура хранения";
            warehouse_dgv.Columns["partOfTheWarehouse"].HeaderText = "Размещение";

        }

        void updateDocumentsDGV()
        {
            var request = @"SELECT * FROM DocumentsTable";
            var adapter = new SqlDataAdapter(request, connectionString);
            DataTable documentsTable = new DataTable();
            adapter.Fill(documentsTable);
            documents_dgv.DataSource = documentsTable;

            documents_dgv.Columns["Id"].Visible = false;
            //warehouse_dgv.Columns["Id1"].Visible = false;
            //warehouse_dgv.Columns["Warehouse_id"].Visible = false;
            //warehouse_dgv.Columns["product_id"].Visible = false;
            //warehouse_dgv.Columns["warehouse_id1"].Visible = false;

            documents_dgv.Columns["Type"].HeaderText = "Тип документа";
            documents_dgv.Columns["Number"].HeaderText = "Номер документа";
            documents_dgv.Columns["Date"].HeaderText = "Дата";
            //warehouse_dgv.Columns["ValidUntil"].HeaderText = "Дата истечения срока годности";
            //warehouse_dgv.Columns["Quantity"].HeaderText = "Количество";
            //warehouse_dgv.Columns["purchasePrice"].HeaderText = "Цена закупки";
            //warehouse_dgv.Columns["StorageTemperature"].HeaderText = "Температура хранения";
            //warehouse_dgv.Columns["partOfTheWarehouse"].HeaderText = "Размещение";

        }

        private void Warehouse_Load(object sender, EventArgs e)
        {
            updateWarehouseDGV();
            updateDocumentsDGV();
            //updateProviderDGV();
            //updatePhoneSearchDGV();
            
        }

     
    }
}
